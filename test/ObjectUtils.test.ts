import "mocha";
import { getSubProperty } from "../src/ObjectUtils";

import { expect } from "chai";

describe("ObjectUtils", () => {

    it("should returns the same object", () => {
        expect(getSubProperty("", { property1: 1 })).to.deep.equal({ property1: 1 });
        expect(getSubProperty<any>("", { property1: 1 })).to.deep.equal({ property1: 1 });
    });

    it("should returns the sub property of object", () => {
        expect(getSubProperty("property1", { property1: 1 })).to.equal(1);
        expect(getSubProperty<number>("property1", { property1: 1 })).to.equal(1);
    });

    it("should returns the sub sub property of object", () => {
        expect(getSubProperty("property1.a", { property1: { a: 1 } })).to.equal(1);
        expect(getSubProperty<number>("property1.a", { property1: { a: 1 } })).to.equal(1);
    });

    it("should returns undefined", () => {
        expect(getSubProperty("property1.b", { property1: { a: 1 } })).to.equal(undefined);
        expect(getSubProperty<any>("property1.b", { property1: { a: 1 } })).to.equal(undefined);
    });

});
